﻿using System;
using System.Threading;

namespace AutoResetEventEx
{
    internal static class Program
    {
        // boolean argument signifies whether the thread is blocked(false) or unblocked(true) by default
        private static AutoResetEvent autoReset_1 = new AutoResetEvent(true);

        private static AutoResetEvent autoReset_2 = new AutoResetEvent(false);

        private static void ThreadProc()
        {
            string name = Thread.CurrentThread.Name;

            Console.WriteLine("{0} waiting on AutoResetEvent #1.", name);
            autoReset_1.WaitOne();
            Console.WriteLine("{0} is released from AutoResetEvent #1.", name);

            Console.WriteLine("{0} waiting on AutoResetEvent #2.", name);
            autoReset_2.WaitOne();
            Console.WriteLine("{0} is released from AutoResetEvent #2.", name);

            Console.WriteLine("{0} ends.", name);
        }

        private static void Main(string[] args)
        {
            for (int i = 0; i < 3; i++)
            {
                Thread t = new Thread(ThreadProc);
                t.Name = "Thread_" + i;
                t.Start();
            }
            Thread.Sleep(250);

            for (int i = 0; i < 2; i++)
            {
                Console.WriteLine("Press Enter to release another thread.");
                Console.ReadLine();
                autoReset_1.Set();
                Thread.Sleep(250);
            }

            Console.WriteLine("\r\nAll threads are now waiting on AutoResetEvent #2.");
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Press Enter to release a thread.");
                Console.ReadLine();
                autoReset_2.Set();
                Thread.Sleep(250);
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}